Esta repositorio contiene los datos actualizados 
de nuestro promedio de encuestas. Puedes consultar la versión web aquí: [Promedio encuestas electorales EL PAÍS](https://elpais.com/especiales/2019/elecciones-generales/encuestas-electorales/). 

Hay dos archivos:

- **encuestas.csv**, que contiene los datos de cada encuesta.
- **promedio.csv**, que contiene la serie con nuestro promedio.

La fuente principal de las encuestas es [Wikipedia](https://en.wikipedia.org/wiki/Opinion_polling_for_the_2019_Spanish_general_election). 
El promedio es un cálculo propio, cuya metodología se detalla en nuestro artículo. 
 
