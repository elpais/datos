Resultados de las elecciones generales del 28 de abril de 2019, por sección censal. 

La fuente de los datos son las diferentes delegaciones del Gobierno, que han facilitado los datos a El País en la semana siguiente a las elecciones. Al tratarse de datos provisionales, puede haber alguna incongruencia con los defnitivos, que [publica](http://www.infoelectoral.mir.es/infoelectoral/min/areaDescarga.html?method=inicio) el Ministerio del Interior. 

A partir de esta información hemos elaborado [este mapa](https://elpais.com/politica/2019/05/01/actualidad/1556730293_254945.html).