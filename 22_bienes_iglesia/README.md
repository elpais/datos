El archivo CSV ha sido extraido de un documento original en PDF, adjunto en el repositorio, elaborado por la Conferencia Episcopal española. El original contiene algunas erratas e incongruencias que no han podido ser depuradas. La categorización original, por páginas, se refleja en la columna "categoría" del CSV. 

El archivo ha sido utilizado para elaborar esta información: https://elpais.com/sociedad/2022-01-27/buscador-estos-son-los-inmuebles-que-la-iglesia-inmatriculo-y-admite-ahora-que-no-le-pertenecen.html
