Archivo CSV con datos de renta por municipio, distrito y sección censal. Los datos proceden de la [estadística experimental del INE](https://www.ine.es/experimental/atlas/exp_atlas_proyecto.pdf), publicada en junio de 2019 por primera vez. Son relativo a la renta de 2016.
En la columna "type" se da la referencia geográfica para cada renta. En la [nota técnica](https://www.ine.es/experimental/atlas/exp_atlas_proyecto.pdf) se encuentran todos los detalles sobre cómo se elabora esta estadística.

En este archivo incluimos además dos columnas, que hemos elaborado nosotros, para establecer en qué centil de renta se encuentra cada sección censal. 
