Recopilación de casos de abusos cometidos por religiosos españoles. Se incluyen casos documentados en sentencias judiciales, denuncias de víctimas y
de las propias congregaciones religiosas. De cada caso hay una descripción, el año en el que aconteció y un enlace a la fuente. [Aquí explicamos](https://elpais.com/sociedad/2019/02/20/actualidad/1550665445_865125.html) cómo definimos como *caso*.


Los datos se actualizan cada semana [en este enlace](https://elpais.com/tag/c/83bc6582c5bc78d42a6dd50510fcc5fc).

La base de datos ha sido elaborada y será actualizada por el Equipo de Datos de El País. Última actualización: 21 de junio de 2019.