**Datos de covid-19 por provincias**

En el repositorio covid-provincias se recopilan datos de la pandemia por provinicas en España, a partir de la información que hace pública cada comunidad autónoma

**data_uniprovs.csv**: Datos de positivos por CPR y fallecidos (acumulados) de las comunidades uniprovinciales