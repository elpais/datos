Este fichero permite ver la evolución de la deuda viva de cada municipio español desde 2011. 
La [fuente original de los datos](http://www.hacienda.gob.es/es-ES/CDI/Paginas/SistemasFinanciacionDeuda/InformacionEELLs/DeudaViva.aspx) es el Ministerio de Hacienda. 
En EL PAÍS hemos unificado los ficheros del ministerio en uno solo, hemos añadido la población municipal (a partir del Padrón Municipal del INE) y hemos calculado
la deuda por habitante. 

Esta base de datos se ha utilizado por primera vez para elaborar [este artículo](https://elpais.com/economia/2019/06/28/actualidad/1561729933_169199.html).