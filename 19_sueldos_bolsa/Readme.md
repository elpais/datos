Esta repositorio contiene datos de retribuciones en empresas de la Bolsa española. Se han utilizado para [elaborar este artículo](https://elpais.com/economia/2019/04/12/actualidad/1555064985_358381.html).
Hay dos archivos:


**Ibex35.csv**, que contiene los datos del sueldo medio de los trabajadores de las empresas que cotizan en el Ibex 35 y el salario del ejecutivo mejor pagado de cada empresa.

**Resto_cotizadas.csv**, que contiene los datos del sueldo medio de los trabajadores del resto de empresas cotizadas y el salario del ejecutivo mejor pagado de cada empresa.

La fuente de los datos son los informes corporativos que cada compañía remite a la Comisión Nacional del Mercado de Valores.