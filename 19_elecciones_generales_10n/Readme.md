
## Resultados del 10N

Resultados de las elecciones generales del 10 de noviembre 2019, 
por municipio y por sección censal.

- **Municipios**. Son datos del escrutinio al 99% y no incluyen el voto por correo.

- **Sección censal**. Resultados provisionales proporcionados a EL PAÍS por
las diferentes delegaciones del Gobierno. Usamos esos datos para elaborar
este [mapa con el voto calle a calle](https://elpais.com/politica/2019/11/11/actualidad/1573498548_290179.html).

**¿Datos definitivos?** Los publicará el Ministerio de Interior [aquí](http://infoelectoral.mir.es/).